module.exports = {
  env: {
    'react-native/react-native': true,
    es6: true,
  },
  extends: ['plugin:react/recommended', 'plugin:react-native/all', 'standard'],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: ['react', 'react-native', 'prettier'],
  parser: 'babel-eslint',
  rules: {
    indent: 'off',
    'react-native/no-raw-text': 0, // Avoid false positive, wait for fix
    semi: [2, 'always'],
    quotes: "off",
    'generator-star-spacing': [2, 'after'],
    'space-before-function-paren': [
      2,
      {
        anonymous: 'always',
        named: 'always',
        asyncArrow: 'always',
      },
    ],
    'react-native/sort-styles': 'off'
  },
}
