/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App/App';
import { name as appName } from './app.json';
import BackgroundMsg from './App/BackgroundMsg';

AppRegistry.registerComponent(appName, () => App);
AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => BackgroundMsg);
