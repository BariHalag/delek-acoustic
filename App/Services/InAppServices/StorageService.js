import AsyncStorage from '@react-native-community/async-storage';
// import { bugsnag } from '../HttpService/BugsnagService';
import { USER_STORAGE_KEY } from './StorageService.Constants';

export async function setItem (key, value) {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    // bugsnag.notify(new Error(`Could not save to local storage: ${key}`));
    console.log(e);
  }
}

export async function getItem (key) {
  try {
    return AsyncStorage.getItem(key);
  } catch (e) {
    // bugsnag.notify(new Error(`Could not get from local storage: ${key}`));
    console.log(e);
  }
}

export async function setCredentials (value) {
  try {
    const credentials = value && JSON.stringify(value);
    if (credentials == null) {
      return AsyncStorage.removeItem(USER_STORAGE_KEY);
    }
    return await AsyncStorage.setItem(USER_STORAGE_KEY, credentials);
  } catch (e) {
    // bugsnag.notify(new Error(`Could not setCredentials to local storage: ${USER_STORAGE_KEY}`));
    console.log(e);
  }
}

export async function getCredentials () {
  try {
    return AsyncStorage.getItem(USER_STORAGE_KEY);
  } catch (e) {
    // bugsnag.notify(new Error(`Could not setCredentials to local storage: ${USER_STORAGE_KEY}`));
    console.log(e);
  }
}
