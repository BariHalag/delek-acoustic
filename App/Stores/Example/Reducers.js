/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { ExampleTypes } from './Actions';

export const updateFcmToken = (state, { fcm }) => ({
  ...state,
  fcmToken: fcm
});

export const updateNotifications = (state, { notification }) => ({
  ...state,
  notifications: notification
});

export const updatePushData = (state, { data }) => ({
  ...state,
  pushData: data
});

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [ExampleTypes.UPDATE_FCM_TOKEN]: updateFcmToken,
  [ExampleTypes.UPDATE_NOTIFICATIONS]: updateNotifications,
  [ExampleTypes.UPDATE_PUSH_DATA]: updatePushData
});
