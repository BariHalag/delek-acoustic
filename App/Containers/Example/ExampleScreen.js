import React from 'react';
import { Text, View } from 'react-native';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { Helpers } from 'App/Theme';

class ExampleScreen extends React.Component {
  render() {
    const { fcmToken, notifications } = this.props;
    console.log(notifications);
    return (
      <View
        style={[Helpers.fill, Helpers.center]}
      >
        <Text>FCM Check</Text>
        <Text>{fcmToken}</Text>
        <Text>Notification</Text>
        <Text>Function: {notifications.function}</Text>
      </View>
    );
  }
}

ExampleScreen.propTypes = {
  fcmToken: PropTypes.string,
  notifications: PropTypes.object,
  pushData: PropTypes.array
};

const mapStateToProps = (state) => ({
  fcmToken: state.example.fcmToken,
  notifications: state.example.notifications,
  pushData: state.example.pushData
});

const mapDispatchToProps = () => ({
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExampleScreen);
