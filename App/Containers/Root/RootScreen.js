import React, { Component } from 'react';
import NavigationService from '../../Services/NavigationService';
import AppNavigator from 'App/Navigators/AppNavigator';
import { View, Platform, BackHandler, NativeModules, NativeEventEmitter } from 'react-native';
import { connect } from 'react-redux';
import StartupActions from 'App/Stores/Startup/Actions';
import { PropTypes } from 'prop-types';
import { Helpers } from 'App/Theme';
import firebase from 'react-native-firebase';
import { getItem, setItem } from '../../Services/InAppServices/StorageService';
import { FB_TOKEN } from '../../Services/InAppServices/StorageService.Constants';
import ExampleTypes from '../../Stores/Example/Actions';
import SplashScreen from 'react-native-splash-screen';

const RNAcousticMobilePush = NativeModules.RNAcousticMobilePush;
RNAcousticMobilePush.requestPushPermission();
const RNAcousticMobilePushEmitter = new NativeEventEmitter(RNAcousticMobilePush);

class RootScreen extends Component {
  constructor (props) {
    super(props);
    this.subscriptions = [];
  }

  async handleFireBaseRegistrationToken () {
    const { updateFcmToken } = this.props;
    let fcmToken = await getItem(FB_TOKEN);
    console.log('async', fcmToken);
    updateFcmToken(fcmToken);
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      console.log('new', fcmToken);
      updateFcmToken(fcmToken);
      if (fcmToken) {
        setItem(FB_TOKEN, fcmToken);
      }
    }
  }

  async checkPermission () {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.handleFireBaseRegistrationToken();
    } else {
      this.handleRequestPermission();
    }
  }

  async handleRequestPermission () {
    try {
      await firebase.messaging().requestPermission();
    } catch (error) {
      console.log('handleRequestPermission', error);
    } finally {
      this.handleFireBaseRegistrationToken();
    }
  }

  handlePushData (data) {
    console.log('push data', data);
  }

  async createNotificationListeners () {
    const { updateNotifications } = this.props;
    firebase.notifications().onNotification((notification) => {
      const notificationToBeDisplayed = new firebase.notifications.Notification()
        .setNotificationId(notification.notificationId)
        .setTitle(notification.title)
        .setBody(notification.body)
        .setSound('default')
        .setData(notification.data);

      if (Platform.OS === 'android') {
        notificationToBeDisplayed.android
          .setPriority(firebase.notifications.Android.Priority.High)
          .android.setChannelId('insider')
          .android.setVibrate(1000);
      }

      updateNotifications({ function: 'onNotification', payload: notificationToBeDisplayed });

        firebase.notifications().displayNotification(notificationToBeDisplayed);
    });

    firebase.messaging().onMessage((message) => {
      // Process your message as required
      console.log('firebase message: ', message);
      updateNotifications({ function: 'onMessage', payload: message });
      this.handlePushData(message.data.router_msg);
    });

    firebase.notifications().onNotificationOpened((notificationOpen) => {
      updateNotifications({ function: 'onNotificationOpened', payload: notificationOpen });
    });

    firebase
      .notifications()
      .getInitialNotification()
      .then((notificationOpen) => {
        updateNotifications({ function: 'getInitialNotification', payload: notificationOpen });
        if (notificationOpen) {
          this.handlePushData(notificationOpen.notification.data.router_msg);
        }
      });
  }

  // MA Function Section
  async update () {
    RNAcousticMobilePush.registrationDetails().then((registrationDetails) => {
      if (typeof (registrationDetails.userId) !== 'undefined' && registrationDetails.userId != null && typeof (registrationDetails.channelId) !== 'undefined' && registrationDetails.channelId) {
        console.log({ registration: 'Finished', userId: registrationDetails.userId, channelId: registrationDetails.channelId });
      }
    }).catch((error) => {
      console.log('MA Error', error);
    });
  }

  maRegisterListener () {
    const { updateNotifications } = this.props;
    this.subscriptions.push(RNAcousticMobilePushEmitter.addListener('Registered', () => {
      this.update();
    }));
    this.subscriptions.push(RNAcousticMobilePushEmitter.addListener('RegistrationChanged', () => {
      this.update();
    }));
    this.subscriptions.push(RNAcousticMobilePushEmitter.addListener("PushReceived", function (payload) {
      updateNotifications({ function: 'RNAcousticMobilePushEmitter', payload: payload });
      console.log("==>>Got a push with payload", payload);
    }));
  }

  handleBackButton () {
    return false;
  }

  componentDidMount () {
    // Run the startup saga when the application is starting
    SplashScreen.hide();
    BackHandler.addEventListener('hardwareBackPress', () => this.handleBackButton());
    const channel = new firebase.notifications.Android.Channel(
      'insider',
      'insider channel',
      firebase.notifications.Android.Importance.Max
    );
    firebase.notifications().android.createChannel(channel);
    this.checkPermission();
    this.createNotificationListeners();
    this.update();
    this.maRegisterListener();
    RNAcousticMobilePush.manualInitialization();
    this.props.startup();
  }

  render () {
    return (
      <View style={Helpers.fill}>
        <AppNavigator
          ref={(navigatorRef) => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </View>
    );
  }
}

RootScreen.propTypes = {
  startup: PropTypes.func,
  updateFcmToken: PropTypes.func,
  updateNotifications: PropTypes.func,
  updatePushData: PropTypes.func
};

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = (dispatch) => ({
  startup: () => dispatch(StartupActions.startup()),
  updateFcmToken: (fcm) => dispatch(ExampleTypes.updateFcmToken(fcm)),
  updateNotifications: (notification) => dispatch(ExampleTypes.updateNotifications(notification)),
  updatePushData: (data) => dispatch(ExampleTypes.updatePushData(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RootScreen);
